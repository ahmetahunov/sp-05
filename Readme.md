https://gitlab.com/ahmetahunov/sp-05
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven
+ PostgreSQL
+ Tomcat

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/sp-05.git
cd sp-05
mvn clean install
```

## run app
```bash
mvn clean install tomcat7:run-war
```
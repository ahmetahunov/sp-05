package ru.ahmetahunov.sp.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import ru.ahmetahunov.sp.entity.Project;
import javax.persistence.QueryHint;
import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, String> {

	@Nullable
	@QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
	public Project findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

	@NotNull
	@QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
	public List<Project> findAllByUserId(@NotNull final String userId);

	public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}

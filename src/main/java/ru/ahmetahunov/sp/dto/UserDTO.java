package ru.ahmetahunov.sp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.sp.enumerated.RoleType;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

	@NotNull
	private String login = "";

	@NotNull
	private RoleType[] roles;

	@NotNull
	private String passwordNew = "";

	@NotNull
	private String password = "";

	@NotNull
	private String passwordCheck = "";

}
